<?php

/**
* Theme supports
*/
add_theme_support( 'title-tag' );

add_theme_support( 'post-thumbnails' ); 


/**
 * Register our sidebars and widgetized areas.
 */
function bcl_widgets_init() {

	register_sidebar( array(
		'name'          => 'Home right sidebar',
		'id'            => 'home_right_1',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => 'Footer latest posts',
		'id'            => 'footer_latest_posts',
		'before_widget' => '<div class="footer-widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<p class="footer-title">',
		'after_title'   => '</p>',
	) );
}
add_action( 'widgets_init', 'bcl_widgets_init' );

/**
* Functions
*/
function register_my_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
  register_nav_menu('services_menu',__( 'Services Menu' ));
  register_nav_menu('resources_menu',__( 'Resources Menu' ));
  register_nav_menu('city_menu',__( 'City Menu' ));
  register_nav_menu('sitemap',__( 'Sitemap' ));
}
add_action( 'init', 'register_my_menu' );

function custom_excerpt_length( $length ) {
	return 100;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );




/**
* Yoast - ACF integration
* Changes the post content before wp seo analyzes the content.
*
* @param string $post_content The post content
*
* @param object $post
*
* @return string $post_content the new post content to be analyzed.
*/
function se_new_content( $post_content, $post )
{
	if ($post->post_title == 'Business Loans')
	{
		$post_content .= get_field( 'home_banner_title', $post->ID) . '<br>';
		$post_content .= get_field( 'home_banner_tagline', $post->ID) . '<br>';
		$post_content .= get_field( 'home_second_section_title', $post->ID) . '<br>';
		$post_content .= get_field( 'home_second_section_claim', $post->ID) . '<br>';

		$post_content .= get_field( 'home_third_section_title', $post->ID) . '<br>';
		$post_content .= get_field( 'call_to_action', $post->ID) . '<br>';
		if( have_rows('home_second_section_boxes') ):
		     // loop through the rows of data
		    while ( have_rows('home_second_section_boxes') ) : the_row();
	        	$post_content .= get_sub_field( 'picture' ).'<br>';
	        	$post_content .= get_sub_field( 'title' ).'<br>';
	        	$post_content .= get_sub_field( 'text' ).'<br>';
		    endwhile;
		endif;	
		if( have_rows('home_third_section_boxes') ):
		     // loop through the rows of data
		    while ( have_rows('home_third_section_boxes') ) : the_row();
	        	$post_content .= get_sub_field( 'title' ).'<br>';
	        	$post_content .= get_sub_field( 'text' ).'<br>';
		    endwhile;
		endif;	
	}

	if ($post->post_title == 'Cities')
	{
		if( have_rows('cities') ):
		     // loop through the rows of data
		    while ( have_rows('cities') ) : the_row();
	        	$post_content .= get_sub_field( 'name' ).'<br>';
	        	$post_content .= get_sub_field( 'picture' ).'<br>';
		    endwhile;
		endif;	
	}

	if ($post->post_title == 'Services' || $post->post_title == 'Resources')
	{
		if( have_rows('cities') ):
		     // loop through the rows of data
		    while ( have_rows('cities') ) : the_row();
	        	$post_content .= get_sub_field( 'title' ).'<br>';
	        	$post_content .= get_sub_field( 'icon' ).'<br>';
	        	$post_content .= get_sub_field( 'text' ).'<br>';
		    endwhile;
		endif;	
	}

	return $post_content;
}
add_filter( 'wpseo_pre_analysis_post_content', 'se_new_content', 10, 2 );










