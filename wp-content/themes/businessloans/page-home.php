<?php /* Template Name: Home */ ?>

<?php get_header('home'); ?>

<section id="home-banner">
	<div class="container">
	<div class="col-md-6">
		<h1 class="section-caption"><?php the_field('home_banner_tagline'); ?></h1>
		<h2 class="section-title"><?php the_field('home_banner_title'); ?></h2>
		<a id="home-banner-buttom" href="<?php echo home_url(); ?>/contact" class="btn btn-red">APPLY NOW</a>
	</div>
	<div class="col-md-6">
		<?php include 'partials/form-home.php'; ?>
	</div>
	</div>

</section>

<section class="business-loans"	style="background-color: #f1f1f1;">
	<div class="container">
		<p class="home-titles-about">ABOUT US</p>
		<p class="home-titles-blue"><?php the_field('home_second_section_title'); ?></p>
		<div class="section-paragraph"><?php the_field('home_second_section_claim'); ?></div>
	</div>
</section>

<section class="business-loans">
	<div class="container">
		<p class="home-titles-about">OUR SERVICES</p>
		<p class="home-titles-blue">Loans For All Business Sizes</p>
		<?php if(get_field('home_second_section_boxes')): ?>

    		<?php while(the_repeater_field('home_second_section_boxes')): ?>
				<div class="col-md-4 home-item-container" >
					<div class="home-item" style="background:url(<?php the_sub_field('picture'); ?>) no-repeat; background-size:cover">
						<div class="item-body">
							<p class="home-bls-title-boxes"><?php the_sub_field('title'); ?></p>
							<?php the_sub_field('text'); ?>
							<a class="read-more" href="<?php the_sub_field('link'); ?>">Read More</a>
						</div>
					</div>
			</div>
    		<?php endwhile; ?>

 		<?php endif; ?>
		<div style="text-align:center;clear:both; padding-top: 40px;"><a href="<?php echo home_url(); ?>/contact" class="btn btn-red">APPLY NOW</a></div>
	</div>
</section>

<section class="what-we-offer">
	<div class="container">
		<p class="home-titles-why">WHY CHOOSE US</p>
		<p class="home-titles-white"><?php the_field('home_third_section_title'); ?></p>
		<p class="section-paragraph"><?php the_field('home_third_section_claim'); ?></p>

		<?php if(get_field('home_third_section_boxes')): ?>

    		<?php while(the_repeater_field('home_third_section_boxes')): ?>
				<div class="col-md-4 col-sm-12 offer-item-container">
					<div class="offer-item">
						<div class="offer-item-body">
							<p class="home-wwo-title-boxes"><?php the_sub_field('title'); ?></p>
							<?php the_sub_field('text'); ?>
<!-- 							<div class="read-more-container">
								<a class="read-more-btn" href="<?php the_sub_field('link'); ?>">Read More</a>								
							</div> -->
						</div>
					</div>
				</div>
    		<?php endwhile; ?>

 		<?php endif; ?>

		<div style="text-align:center;clear:both;"><a href="<?php echo home_url(); ?>/contact" class="btn btn-red">APPLY NOW</a></div>
	</div>
</section>

<section class="testimonial-section" style="text-align:center;">
<p class="home-titles-about">TESTIMONIAL</p>
<p class="home-titles-blue">What Our Clients Say</p>
<div class="container"><?php the_field('testimonial'); ?></div>
</section>

<section class="call-to-action">
	<div class="container">
		<p class="home-titles-why">LET’S TALK TODAY</p>
		<h3><?php the_field('call_to_action'); ?></h3>
		<div style="text-align:center"><a href="<?php echo home_url(); ?>/contact" class="btn btn-red">APPLY NOW</a></div>
	</div>
	
</section>

<?php get_footer(); ?>











