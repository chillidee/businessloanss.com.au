<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div class="blog-header">
	<div class="container">
		<p class="breadcrumb"><a href="/blog">Blog</a> &rarr; <?php the_title(); ?></p>
		<p class="blog-title"><?php the_title(); ?></p>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-lg-9" id="post-list">
			<div class="post">
				<?php the_post_thumbnail(); ?>

				<h1><?php the_title(); ?></h1>
				<div class="metadata">
					<span class="date"><?php the_date(); ?></span> <span class="author">by <?php the_author(); ?></span> - <span class="comments read-more"><?php comments_number(); ?></span>
				</div>
				<div class="post-content"><?php the_content(); ?></div>
				<p><?php the_tags( '<ul class="post-tags"><li>Tags: </li><li>', '</li><li>', '</li></ul>' ); ?></p>
			</div>

			<section class="author-bio" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
				<?php echo get_avatar( get_the_author_meta('user_email'), '80', '' ); ?>
				<h4 class="author-box-title">
					<span itemprop="name"><a href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>"><?php the_author(); ?></a></span>
				</h4>
				<div class="author-box-content" itemprop="description">
					<?php the_author_meta('description'); ?>
				</div>
			</section>
	
			<div class="row relatedposts">
				<h3>Related posts</h3>
				<?php
				$orig_post = $post;
				global $post;
				$tags = wp_get_post_tags($post->ID);
				
				if ($tags) {
					$tag_ids = array();
					foreach($tags as $individual_tag)
						$tag_ids[] = $individual_tag->term_id;

				$args=array(
					'tag__in' 			=> $tag_ids,
					'post__not_in' 		=> array($post->ID),
					'posts_per_page'	=> 3,
					'caller_get_posts'	=> 1
				);
				
				$my_query = new wp_query( $args );
				
				while( $my_query->have_posts() ) {
					$my_query->the_post();
				?>
				
				<div class="col-sm-4 relatedthumb">
					<a rel="external" href="<? the_permalink()?>"><?php the_post_thumbnail([300,300]); ?><br />
						<?php the_title(); ?>
					</a>
				</div>
				
				<?php }
				}
				$post = $orig_post;
				wp_reset_query();
				?>
			</div>		

			<div id="comments-container">
				<?php comments_template(); ?>				
			</div>

		<?php endwhile; else : ?>
			<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
		<?php endif; ?>

		</div>
		<div class="col-lg-3 hidden-md hidden-sm hidden-xs">
			<?php if ( is_active_sidebar( 'home_right_1' ) ) : ?>
				<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
					<?php dynamic_sidebar( 'home_right_1' ); ?>
				</div><!-- #primary-sidebar -->
			<?php endif; ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>