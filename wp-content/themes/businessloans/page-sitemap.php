<?php get_header(); ?>
<div class="category-header">
	<div class="container">
		<h1>SITEMAP</h1>
	</div>
</div>

<div class="container sitemap">
	<?php
		wp_nav_menu( array(
			'menu' => 'Sitemap',
			'container' => false,
		));
	?>
</div>
	

<?php get_footer(); ?>