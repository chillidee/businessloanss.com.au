<?php get_header(); ?>
<div class="blog-header">
	<div class="container">
		<h1>Blog</h1>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-lg-9" id="post-list">
		<?php $count = 0; ?>
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>				
			<?php $count++; ?>
			<div class="post">
				<?php
					if ($count == 1)
					{
						the_post_thumbnail();
					}
				?>
				<p class="blog-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
				<div class="metadata">
					<span class="date"><?php the_date(); ?></span> <span class="author">by <?php the_author(); ?></span> - <span class="comments read-more"><?php comments_number(); ?></span>
				</div>

				<p><?php the_excerpt(); ?></p>
				<p>
					<a href="<?php the_permalink(); ?>" class="read-more">READ MORE</a>
				</p>
			</div>
	

		<?php endwhile; ?>

		<div class="pagination">
			<div class="nav-previous alignleft"><?php next_posts_link( 'Older posts' ); ?></div>
			<div class="nav-next alignright"><?php previous_posts_link( 'Newer posts' ); ?></div>	
		</div>


		<?php else : ?>
			<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
		<?php endif; ?>

		</div>
		<div class="col-lg-3 hidden-md hidden-sm hidden-xs">
			<?php if ( is_active_sidebar( 'home_right_1' ) ) : ?>
				<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
					<?php dynamic_sidebar( 'home_right_1' ); ?>
				</div><!-- #primary-sidebar -->
			<?php endif; ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>