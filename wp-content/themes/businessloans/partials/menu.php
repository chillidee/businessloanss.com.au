	<nav class="navbar navbar-default navbar-fixed-top transparentheader">
		<div class="container">
			<div class="phone-number row">
				<img class="phone-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/icons/phone.png" alt="Call us!">
				<a href="tel:1300886996">1300 886 996</a>
			</div>
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-nav" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo home_url(); ?>">
					<img src="/wp-content/themes/businessloans/img/BusinessLoan_NewLogo_v2.png" alt="Business Loans">
				</a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
 
			<div class="collapse navbar-collapse" id="top-nav">
			<?php
			wp_nav_menu( array(
				'menu' => 'Header Menu',
				'depth' => 2,
				'container' => false,
				'menu_class' => 'nav navbar-nav navbar-right',
			));
			?>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>

