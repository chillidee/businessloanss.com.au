function filippoTestFocusKw() {
    var a = jQuery.trim(jQuery("#" + wpseoMetaboxL10n.field_prefix + "focuskw").val());
    a = ystEscapeFocusKw(a).toLowerCase();
    var b, c;
    jQuery("#editable-post-name-full").length && (b = jQuery("#editable-post-name-full").text(), c = wpseoMetaboxL10n.wpseo_permalink_template.replace("%postname%", b).replace("http://", ""));
    var d = new RegExp("(^|[ \\s\n\r	\\.,'\\(\"\\+;!?:\\-])" + ystRemoveLowerCaseDiacritics(a) + "($|[\\s\n\r	.,'\\)\"\\+!?:;\\-])", "gim"),
        e = ystRemoveLowerCaseDiacritics(a),
        f = new RegExp(e.replace(/\s+/g, "[-_\\//]"), "gim"),
        g = jQuery("#focuskwresults"),
        h = jQuery("#wpseosnippet").find(".desc span.content").text();
    if ("" !== a) {
        var i = "<p>" + wpseoMetaboxL10n.keyword_header + "</p>";
        var acfContent = '';
        jQuery('.field').each(function(index, el) {
        	var input = jQuery(this).find('input');
        	var textarea = jQuery(this).find('textarea');
        	if(input.length > 0)
        		acfContent += input.val() + ' ';
        	if(textarea.length > 0)
        		acfContent += textarea.val() + ' ';
        });
        console.log(acfContent);
        var totalContent = jQuery('#content').val() + acfContent;

        i +=    "<ul>",
                jQuery("#title").length && 
                (i += "<li>" + wpseoMetaboxL10n.article_header_text + ystFocusKwTest(jQuery("#title").val(), d) + "</li>"),
                 i += "<li>" + wpseoMetaboxL10n.page_title_text + ystFocusKwTest(jQuery("#wpseosnippet_title").text(), d) + "</li>", 
                 i += "<li>" + wpseoMetaboxL10n.page_url_text + ystFocusKwTest(c, f) + "</li>", 
                 jQuery("#content").length && (i += "<li>" + wpseoMetaboxL10n.content_text + ystFocusKwTest(totalContent, d) + "</li>"), 
                 i += "<li>" + wpseoMetaboxL10n.meta_description_text + ystFocusKwTest(h, d) + "</li>", 
                 i += "</ul>", g.html(i)
    } else g.html("")
}

(function($){
    $('body').on('click', 'h1', function(event) {
        event.preventDefault();
        console.log('ciao');
    });
    // $('#focuskwresults p').css('color', 'blue');
})(jQuery);