(function ($) {

    // GOOGLE ANALYTICS
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-57484116-1', 'auto');
    ga('send', 'pageview');



    var screenHeight = $(window).height(),
            screenWidth = $(window).width();

    if (screenWidth < 667)
    {
        var menu = $('#menu-header-menu');

        menu.children('li').not(':last').append('<span class="dropdown-button"> > </span>');

        $('.dropdown-button').on('click', function (e) {
            e.preventDefault();
            var $this = $(this),
                    submenu = $this.siblings('.sub-menu');
            if (submenu.is(':visible'))
                $this.text(' > ');
            else
                $this.text(' < ');

            submenu.slideToggle();
        });
    }

    // Set the home page triangle to half width
    setTriangleSize();

    // ALC form fix
    $("#apply-form p:contains('* indicates required information.')").hide();

    resizeFormInputs();

    $(window).resize(function () {
        setTriangleSize();
        resizeFormInputs();
    });

    $('#apply-form input#apiSubmit').val('Apply Now');
    //$('#balanceOwingError, #realEstateValueError').remove();

    $('textarea#comment').attr('required', 'required');

    $('.nav-previous, .nav-next').each(function (index, el) {
        if ($(this).find('a').length == 0)
            $(this).hide();
    });

})(jQuery);

function setTriangleSize()
{
    var screenWidth = $(window).width();
    $('.triangle-left').css('border-left-width', screenWidth / 3);
    $('.triangle-left').css('border-top-width', screenWidth / 10);

    $('.triangle-right').css('border-left-width', screenWidth / 3);
    $('.triangle-right').css('border-bottom-width', screenWidth / 10);
}

function resizeFormInputs()
{
    var applyForm = $('#apply-form'),
            formWidth = applyForm.find('.form-body').width(),
            labelWidth = applyForm.find('label').outerWidth(),
            remainingWidth = formWidth - labelWidth - 20;

    applyForm.find('select, input[type=text]').css('width', remainingWidth);
}



