<?php /* Template Name: Test */ ?>

<?php get_header(); ?>
	
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<div class="category-header">
		<div class="container">
			<h1><?php the_title(); ?></h1>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="page-content">
				<div class="col-md-6 col-lg-4 form-container">
					<?php include 'partials/testform.php'; ?>
				</div>

				<div class="col-md-6 col-lg-8 contact">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>

<?php endwhile; else : ?>
	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

<?php get_footer(); ?>