<?php get_header(); ?>
	
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<div class="category-header">
		<div class="container">
			<h1><?php the_title(); ?></h1>
		</div>
	</div>
	
	<div class="container">
		<div class="page-content">
			<?php the_content(); ?>				
		</div>
	</div>

<?php endwhile; else : ?>
	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

<?php get_footer(); ?>