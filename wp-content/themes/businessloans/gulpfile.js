var gulp = require('gulp');
var csslint = require('gulp-csslint');
var concat = require('gulp-concat');
var csso = require('gulp-csso');
var rev = require('gulp-rev');
var rename = require('gulp-rename');

gulp.task('css',function(){
	gulp.src([
			'css/bootstrap.min.css',
			'css/basic-layout.css',
			'css/utilities.css',

			'css/layout/blog.css',
			'css/layout/cities.css',
			'css/layout/content.css',
			'css/layout/default-header.css',
			'css/layout/footer.css',
			'css/layout/home-header.css',

			'css/components/apply-form.css',
			'css/components/category-box.css',
			'css/components/comments.css',
			'css/components/home-box.css',
			'css/components/top-menu.css',
			'css/components/triangle.css',
			'css/components/widgets.css',
		])
		.pipe(csslint())
		.pipe(concat({path:'build.min.css',cwd:''}))
		.pipe(csso())
		.pipe(rev())
		.pipe(gulp.dest('css/builds'))
        .pipe(rev.manifest())
        .pipe(rename('css-manifest.json'))
		.pipe(gulp.dest('css'));
});

gulp.task('default', function(){
 
    console.log('default gulp task...')
 
});
gulp.task('watch',function(){
	gulp.watch(['css/**/*.css','!css/builds/*.css'],['css']);
});