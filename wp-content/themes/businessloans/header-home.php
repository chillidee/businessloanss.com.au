<!DOCTYPE html>
<html>
	<head <?php language_attributes(); ?>>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Business Loans - Low Doc Loans - Business Loans Australia</title>
		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
		<?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
		<script async src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<link href='https://fonts.googleapis.com/css?family=Muli:400,400italic%7CMontserrat%7COpen+Sans:400,600' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css">		
    <script>
      $(document).scroll(function(){100<$(this).scrollTop()?($("#transparentmenu").hide(),$(".hiddenclass").show()):($("#transparentmenu").show(),$(".hiddenclass").hide())}),$(function(){$("#postCode").autocomplete({minLength:3,source:function(o,t){$.ajax({type:"POST",url:"http://businessloanss.com.au/auspost/auspost.php",dataType:"json",data:{postcode:o.term},success:function(o){o.localities.locality instanceof Array?t($.map(o.localities.locality,function(o){return{label:o.postcode+", "+o.location,value:o.postcode+", "+o.location,location:o.location,postcode:o.postcode,state:o.state}})):t($.map(o.localities,function(o){return{label:o.postcode+", "+o.location,value:o.postcode+", "+o.location,location:o.location,postcode:o.postcode,state:o.state}}))}})},select:function(o,t){$("#field1").val(t.item.location),$("#field2").val(t.item.postcode),$("#field3").val(t.item.state)}})});
    </script>
		<!-- Global Site Tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-57484116-1"></script>
		<script>
  		function gtag(){dataLayer.push(arguments)}window.dataLayer=window.dataLayer||[],gtag("js",new Date),gtag("config","UA-57484116-1");
		</script>
		<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/bsl_favicon_16x16_3zq_icon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/bsl_favicon_16x16_3zq_icon.ico" type="image/x-icon">
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<header class="home-header">
				<?php include 'partials/menu-home.php'; ?>
		</header>