<?php get_header(); ?>
	
	<div class="category-header">
		<div class="container">
			<h1>AUSTRALIA WIDE</h1>
		</div>
	</div>

	<div id="category-items" class="container">
		<div class="row">

		<?php if(get_field('services_resources_cities')): ?>

		    <?php while(the_repeater_field('services_resources_cities')): ?>
				<div class="col-md-4 col-sm-6">
					<div class="category-item city-item">
						<a href="<?php the_sub_field('link'); ?>">
							<img src="<?php the_sub_field('icon'); ?>" alt="<?php the_sub_field('title'); ?>">
							<p class="category-box-titles"><?php the_sub_field('title'); ?></p>
							<p><?php the_sub_field('text'); ?></p>
							<p class="read-more-btn">Read More</p>
						</a>
					</div>
				</div>
		    <?php endwhile; ?>

		<?php endif; ?>
		</div>
	</div>
	


<?php get_footer(); ?>