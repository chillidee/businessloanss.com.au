	<footer id="main-footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-3">
					<a href="/services"><p class="footer-headings">Services</p></a>
					<?php
					wp_nav_menu( array(
						'menu' => 'Services Menu',
						'container' => false,
					));
					?>
				</div>
				<div class="col-sm-6 col-md-3">
					<a href="/resources"><p class="footer-headings">Resources</p></a>
					<?php
					wp_nav_menu( array(
						'menu' => 'Resources menu',
						'container' => false,
					));
					?>
				</div>
				<div class="col-sm-6 col-md-3">
					<a href="/business-loans-australia/"><p class="footer-headings">Business Loans Australia</p></a>
					<a href="/contact/" class="btn btn-red" style="margin-top: 10px;">APPLY NOW</a>
				</div>
				<div class="col-sm-6 col-md-3 latest-news">
					<a href="/blog"><p class="footer-headings">Latest News</p></a>
					<?php if ( is_active_sidebar( 'footer_latest_posts' ) ) : ?>
							
							<?php dynamic_sidebar( 'footer_latest_posts' ); ?>

					<?php endif; ?>
				</div>
			</div>
		</div>
	

		<div id="copy">
			Copyright &copy; 2018 | <a href="<?php echo home_url(); ?>/sitemap">Sitemap</a>
		</div>
	</footer>


	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/scripts.js"></script>
	<?php wp_footer(); ?>
</body>
</html>