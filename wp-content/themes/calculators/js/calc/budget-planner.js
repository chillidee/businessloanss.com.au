(function ($) {
  var $tabContainers = $('.tab-container');
  var $pageNumber = $('#pageNumber');
  var $nav = $('.nav-menu');
  var $navTabs = $nav.children('li');
  var $summaryCategories = $('.summary-category');
  var $tabsArrow = $('.prev-tab, .next-tab');
  var categoriesTotalSum = {};
  var currentPage = 1;
  var finalResult = [];
  var resultsCategory = [
    'Total Income',
    'Vehicle/ Transport',
    'Living Expenses',
    'Insurance/ Super',
    'Loans, Credit/ Store Cards',
    'Taxes, Fees & Charges',
    'Leisure/ Entertainment'
  ];

  /**
   * @param value
   * @param frequency
   * @returns {number}
   */
  function calcIncome (value, frequency) {
    frequency = [1, MONTHS_PER_YEAR, FORTNIGHTS_PER_YEAR, WEEKS_PER_YEAR][frequency];
    return value * frequency;
  }

  /**
   * @param {string} category
   * @returns {number}
   */
  function calCategoryTotal (category) {
    var results = 0;
    $.each(categoriesTotalSum['c' + category], function (index, data) {
      results += data.income;
    });
    return results;
  }

  function calcTotals () {
    var i = 0;

    var expenses = {
      weekly: 0,
      monthly: 0,
      income: 0
    };

    var summary = {
      weekly: 0,
      monthly: 0,
      income: 0
    };

    finalResult = [];

    $.each(categoriesTotalSum, function (index, cols) {
      var $categoryRow = $summaryCategories.eq(i);
      var $cols = $categoryRow.children('div');
      var colIndex = 0;
      var totals = {
        weekly: 0,
        monthly: 0,
        income: 0
      };

      $.each(cols, function (index, data) {
        totals.weekly += data.weekly;
        totals.monthly += data.monthly;
        totals.income += data.income;
      });

      $.each(totals, function (field, value) {
        if (i === 0) {
          summary[field] += value;
        } else {
          expenses[field] += value;
        }
        $cols.eq(colIndex).find('p').text(LoanCalc.addCommas(value));
        colIndex++;
      });

      finalResult.push({key: resultsCategory[i], value: ''});
      finalResult.push({key: 'Weekly', value: '$' + LoanCalc.addCommas(totals.weekly)});
      finalResult.push({key: 'Monthly', value: '$' + LoanCalc.addCommas(totals.monthly)});
      finalResult.push({key: 'Annual', value: '$' + LoanCalc.addCommas(totals.income) });
      finalResult.push({key: "SEPARATOR"});

      i++;
    });

    $.each(['Total Expenses', 'SURPLUS'], function (index, label) {
      var data
        , fieldsKey
      ;

      if (index === 0) {
        data = $.extend({}, expenses);
        fieldsKey = 'expenses';
      } else {
        data = $.extend({}, summary);
        fieldsKey = 'surplus';

        if (expenses.monthly > data.monthly || expenses.weekly > data.weekly || expenses.income > data.income) {
          label = 'DIFICIT';
        }

        data.monthly = Math.abs(expenses.monthly - data.monthly);
        data.weekly = Math.abs(expenses.weekly - data.weekly);
        data.income = Math.abs(expenses.income - data.income);
        $('#surplus-label').text(label + ':');
      }

      $('#' + fieldsKey + '-weekly-value').text(LoanCalc.addCommas(data.weekly));
      $('#' + fieldsKey + '-monthly-value').text(LoanCalc.addCommas(data.monthly));
      $('#' + fieldsKey + '-income-value').text(LoanCalc.addCommas(data.income));

      finalResult.push({key: label, value: ''});
      finalResult.push({key: 'Weekly', value: '$' + LoanCalc.addCommas(data.weekly)});
      finalResult.push({key: 'Monthly', value: '$' + LoanCalc.addCommas(data.monthly)});
      finalResult.push({key: 'Annual', value: '$' + LoanCalc.addCommas(data.income)});
      finalResult.push({key: "SEPARATOR"});
    });
  }

  /**
   * @param {jQuery.Event} e
   */
  function onFieldUpdate (e) {
    var $row = $(this).closest('li');
    var $category = $row.closest('[data-category]');
    var category = $category.data('category');

    if (category) {
      var frequency;
      var value;
      var income;
      var total = 0;

      if ('select' === this.tagName.toLowerCase()) {
        frequency = this.selectedIndex;
        value = $row.find('input[type=text]').val();
      } else {
        frequency = $row.find('select').get(0).selectedIndex;
        value = $(this).val();
        $(this).val(LoanCalc.addCommas(value));
      }

      value = parseFloat(value);

      if (isNaN(value)) {
        value = 0;
      }

      income = calcIncome(value, frequency);

      if (!categoriesTotalSum.hasOwnProperty('c' + category)) {
        categoriesTotalSum['c' + category] = {};
      }

      categoriesTotalSum['c' + category][$row.index()] = {
        income: income,
        amount: value,
        weekly: income > 0 ? Math.round(income / WEEKS_PER_YEAR) : 0,
        monthly: income > 0 ? Math.round(income / MONTHS_PER_YEAR) : 0
      };

      $row.find('.amount').text(LoanCalc.addCommas(income));
      $category.find('.total strong').text(LoanCalc.addCommas(calCategoryTotal(category)));
    }

    calcTotals();
  }

  /**
   * @param {number=} page
   */
  function renderTabs(page) {
    var total = $navTabs.length;
    var tabs = [];
    var baseTabs;

    if ('number' !== typeof page || isNaN(page) || page < 1 || total < (page - 1)) {
      page = $nav.find('.nav-menu-activ');
      page = page.length ? parseInt(page.data('nav'), 10) : 1;
    }

    for (var i=0; i<total; i++) {
      tabs.push($navTabs[i]);
    }

    baseTabs = tabs.splice(page > 3 ? 0 : 3, 3);
    currentPage = page;

    $tabsArrow.removeClass('no-activ');
    if (currentPage === 1) {
      $tabsArrow.eq(0).addClass('no-activ');
    } else if (currentPage === 6) {
      $tabsArrow.eq(-1).addClass('no-activ');
    }

    $pageNumber.text(page);
    $tabContainers.eq(page-1).addClass('active-tab').siblings().removeClass('active-tab');
    $.fn.append.apply($nav.empty(), tabs.concat(baseTabs));
    $nav.find('[data-nav="' + page + '"]').addClass('nav-menu-activ').siblings().removeClass('nav-menu-activ')
  }

  renderTabs();

  $(document).
    on('click', '.nav-menu li', function (e) {
      renderTabs(parseInt($(this).data('nav'), 10));
    }).
    on('click', '.prev-tab, .next-tab', function (e) {
      e.preventDefault();
      var $element = $(this);
      var next = currentPage;
      if ($element.hasClass('next-tab')) {
        next += 1;
      } else {
        next -= 1;
      }
      if (!(next < 1 || next > 6)) {
        renderTabs(next);
      }
    }).
    on('change', '[data-category] select, [data-category] input[type=text]', onFieldUpdate).
    on('click', '.calc .email', function (e) {
      e.preventDefault();
      var $form = $('#email-form');
      var isVisible = $form.is(':visible');
      $form[isVisible ? 'hide' : 'show']();
      $(".calc #message").val(''); //.val(isVisible ? '' : LoanCalc.resultToString(finalResult));
    }).
    on('click', '.calc .save', function (e) {
      e.preventDefault();
      var data = encodeURIComponent(JSON.stringify(finalResult));
      var win=window.open('save.php?data=' + data + '&title=Budget Planner', '_blank');
      win.focus();
    }).
    on('click', '.calc .print', function () {
      window.print();
    }).
    on('click', '#reset-fields', function (e) {
      e.preventDefault();
      $('[data-category] input[type=text]').val('').trigger('change');
      $('[data-category] select').each(function () {
        this.selectedIndex = 1;
      });
    })
  ;

  $(".calc #send_email").click(function (e) {
    e.preventDefault();
    LoanCalc.sendMail();
  });

  $('[data-category] select, [data-category] input[type=text]').trigger('change');
})(jQuery);

