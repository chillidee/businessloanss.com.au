jQuery(document).ready(function($) {

	setTimeout(function() {
		var span = $("#focuskwresults li:contains('Content:')").find('span'),
			acfContent = '',
			a = jQuery.trim(jQuery("#yoast_wpseo_focuskw").val()),
			d = new RegExp("(^|[ \\s\n\r	\\.,'\\(\"\\+;!?:\\-])" + ystRemoveLowerCaseDiacritics(a) + "($|[\\s\n\r	.,'\\)\"\\+!?:;\\-])", "gim");

		a = ystEscapeFocusKw(a).toLowerCase();

        $('.field').each(function(index, el) {
        	var $this = $(this),
        		input = $this.find('input'),
        		textarea = $this.find('textarea');
        	if(input.length > 0)
        		acfContent += input.val() + ' ';
        	if(textarea.length > 0)
        		acfContent += textarea.val() + ' ';
        });
		
		totalContent = $('#content').val() + acfContent;

		span.html(ystFocusKwTest(totalContent, d));
	}, 3000);
 	
});