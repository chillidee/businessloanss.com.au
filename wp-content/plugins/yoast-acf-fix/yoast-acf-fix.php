<?php
/**
 * Plugin Name: Filippo's Yoast - ACF fix
 * Description: This plugin lets Yoast analyze your Advanced Custom Fields
 * Version: 1.0.0
 * Author: Filippo Donadoni
 */

add_action( 'admin_enqueue_scripts', 'my_script' );
function my_script()
{
	 wp_enqueue_script( 'yoast-acf-fix', plugin_dir_url( __FILE__ ) . 'yoast-acf-fix.js', ['jquery'] );
}