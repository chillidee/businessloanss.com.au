<?php

class ALCAPIFormSettingsPage {

    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct() {
        add_action('admin_menu', array($this, 'add_plugin_page'));
        add_action('admin_init', array($this, 'page_init'));
    }

    /**
     * Add options page
     */
    public function add_plugin_page() {
        // This page will be under "Settings"
        add_options_page(
                'Settings Admin', 'ALC API Form', 'manage_options', 'alcapiform-setting-admin', array($this, 'create_admin_page')
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page() {
        // Set class property
        $this->options = get_option('alcapiform_option_name');
        ?>
        <div class="wrap">
            <?php screen_icon(); ?>
            <h2>ALC API Form for Money Maker Settings</h2>
            <form method="post" action="options.php">
                <?php
                // This prints out all hidden setting fields
                settings_fields('alcapiform_option_group');
                do_settings_sections('alcapiform-setting-admin');
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init() {

        register_setting(
                'alcapiform_option_group', // Option group
                'alcapiform_option_name', // Option name
                array($this, 'sanitize') // Sanitize
        );

        add_settings_section(
                'setting_section_id', // ID
                'Global Default Settings', // Title
                array($this, 'print_section_info'), // Callback
                'alcapiform-setting-admin' // Page
        );		

        add_settings_field(
                'geturl', 'API Get URL without Company Code', array($this, 'geturl_callback'), 'alcapiform-setting-admin', 'setting_section_id'
        );

        add_settings_field(
                'posturl', 'API Post URL', array($this, 'posturl_callback'), 'alcapiform-setting-admin', 'setting_section_id'
        );

        add_settings_field(
                'cid', 'CID', array($this, 'cid_callback'), 'alcapiform-setting-admin', 'setting_section_id'
        );

        add_settings_field(
                'css', 'Enquiry Form CSS Full URL', array($this, 'css_callback'), 'alcapiform-setting-admin', 'setting_section_id'
        );

        add_settings_field(
                'jquery', 'jQuery Full URL only if jQuery was not loaded', array($this, 'jquery_callback'), 'alcapiform-setting-admin', 'setting_section_id'
        );        

        add_settings_field(
                'Send emails to third party', 'Send emails to third party', array($this, 'send_emails_callback'), 'alcapiform-setting-admin', 'setting_section_id'
        );

        add_settings_field(
                'pssurl', 'Partner Success URL', array($this, 'pssurl_callback'), 'alcapiform-setting-admin', 'setting_section_id'
        );

        add_settings_field(
                'partner_email', 'Partner email address', array($this, 'partner_email_callback'), 'alcapiform-setting-admin', 'setting_section_id'
        );

        add_settings_field(
                'from_email', 'From email address', array($this, 'from_email_callback'), 'alcapiform-setting-admin', 'setting_section_id'
        );

        add_settings_field(
                'company_name', 'Company name to appear on emails', array($this, 'company_name_callback'), 'alcapiform-setting-admin', 'setting_section_id'
        );
		
		add_settings_section(
                'form_section_id', // ID
                'Form Settings', // Title
                array($this, 'print_form_section'), // Callback
                'alcapiform-setting-admin' // Page
        );		
			
		add_settings_field(
                'ask_preferred_time_call_back', 'Ask preferred time for call back', array($this, 'ask_preferred_time_call_back_callback'), 'alcapiform-setting-admin', 'form_section_id'
        );
		
		add_settings_field(
                'privacy_text', 'Privacy Text', array($this, 'privacy_text_callback'), 'alcapiform-setting-admin', 'form_section_id'
        );

        add_settings_field(
                'privacy', 'Privacy Page Url', array($this, 'privacy_callback'), 'alcapiform-setting-admin', 'form_section_id'
        );

        add_settings_field(
                'privacy_link_text', 'Privacy Link Text', array($this, 'privacy_link_text_callback'), 'alcapiform-setting-admin', 'form_section_id'
        );
		
		add_settings_section(
                'sms_section_id', // ID
                'SMS Settings', // Title
                array($this, 'print_sms_section'), // Callback
                'alcapiform-setting-admin' // Page
        );

		add_settings_field(
                'send_sms_customer', 'Send SMS when lead submitted', array($this, 'send_sms_customer_callback'), 'alcapiform-setting-admin', 'sms_section_id'
        );		
		
		add_settings_field(
                'sms_account_id', 'SMS Account ID', array($this, 'sms_account_id_callback'), 'alcapiform-setting-admin', 'sms_section_id'
        );

		add_settings_field(
                'sms_user_email', 'SMS User email', array($this, 'sms_user_email_callback'), 'alcapiform-setting-admin', 'sms_section_id'
        );
		
		add_settings_field(
                'sms_user_password', 'SMS User password', array($this, 'sms_user_password_callback'), 'alcapiform-setting-admin', 'sms_section_id'
        );
		
		add_settings_field(
                'sms_message', 'SMS Message<br/><br/>Pull Enquiry Form fields using {{fieldName}}, for example, {{firstName}} pulls the field First Name', array($this, 'sms_message_callback'), 'alcapiform-setting-admin', 'sms_section_id'
        );
		
		add_settings_section(
                'working_hours_section_id', // ID
                'Working Hours Settings', // Title
                array($this, 'print_working_hours_section'), // Callback
                'alcapiform-setting-admin' // Page
        );	
			
		add_settings_field(
                'opening_hours', 'Opening hours', array($this, 'opening_hours_callback'), 'alcapiform-setting-admin', 'working_hours_section_id'
        );
		
		add_settings_field(
                'lunch_starts', 'Lunch starts at', array($this, 'lunch_starts_callback'), 'alcapiform-setting-admin', 'working_hours_section_id'
        );
		
		add_settings_field(
                'lunch_ends', 'Lunch ends at', array($this, 'lunch_ends_callback'), 'alcapiform-setting-admin', 'working_hours_section_id'
        );
		
		add_settings_field(
                'closing_hours', 'Closing hours', array($this, 'closing_hours_callback'), 'alcapiform-setting-admin', 'working_hours_section_id'
        );
		
		add_settings_field(
                'closing_hours_friday', 'Closing hours on Fridays', array($this, 'closing_hours_friday_callback'), 'alcapiform-setting-admin', 'working_hours_section_id'
        );
		
		add_settings_section(
                'log_section_id', // ID
                'Log Settings', // Title
                array($this, 'print_log_section'), // Callback
                'alcapiform-setting-admin' // Page
        );
			
		add_settings_field(
                'record_leads', 'Record leads in CSV file (log)', array($this, 'record_leads_callback'), 'alcapiform-setting-admin', 'log_section_id'
        );
		
		add_settings_field(
                'send_email_lead_fail', 'Send email when lead fails', array($this, 'send_email_lead_fail_callback'), 'alcapiform-setting-admin', 'log_section_id'
        );	
		
		add_settings_field(
                'email_lead_fail', 'To email address', array($this, 'email_lead_fail_callback'), 'alcapiform-setting-admin', 'log_section_id'
        );
		
		add_settings_field(
                'ignore_customers_lead_fail', 'Ignore leads by First Name', array($this, 'ignore_customers_lead_fail_callback'), 'alcapiform-setting-admin', 'log_section_id'
        );				

        add_settings_section(
                'setting_section_id_usage', // ID
                'Usage Instruction', // Title
                array($this, 'print_section_info_usage'), // Callback
                'alcapiform-setting-admin' // Page
        );

        add_settings_section(
                'setting_section_id_shortcode', // ID
                'Override Instruction', // Title
                array($this, 'print_section_info_shortcode'), // Callback
                'alcapiform-setting-admin' // Page
        );
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize($input) {
        $new_input = array();

		// Global Default Settings ***********************************************************************
		
        if (isset($input['geturl']))
            $new_input['geturl'] = sanitize_text_field($input['geturl']);

        if (isset($input['posturl']))
            $new_input['posturl'] = sanitize_text_field($input['posturl']);

        if (isset($input['cid']))
            $new_input['cid'] = sanitize_text_field($input['cid']);

        if (isset($input['svs']))
            $new_input['svs'] = sanitize_text_field($input['svs']);

        if (isset($input['mode']))
            $new_input['mode'] = sanitize_text_field($input['mode']);

        if (isset($input['css']))
            $new_input['css'] = sanitize_text_field($input['css']);

        if (isset($input['jquery']))
            $new_input['jquery'] = sanitize_text_field($input['jquery']);       

        if (isset($input['pssurl']))
            $new_input['pssurl'] = sanitize_text_field($input['pssurl']);

        if (isset($input['partner_email']))
            $new_input['partner_email'] = sanitize_text_field($input['partner_email']);

        if (isset($input['from_email']))
            $new_input['from_email'] = sanitize_text_field($input['from_email']);

        if (isset($input['company_name']))
            $new_input['company_name'] = sanitize_text_field($input['company_name']);

        if (isset($input['send_emails']))
            $new_input['send_emails'] = $input['send_emails'];
				
		// Form Settings ********************************************************************************
		
		if (isset($input['type_of_loans']))
            $new_input['type_of_loans'] = $input['type_of_loans'];
	
		if (isset($input['ask_preferred_time_call_back']))
            $new_input['ask_preferred_time_call_back'] = $input['ask_preferred_time_call_back'];

		if (isset($input['privacy']))
            $new_input['privacy'] = sanitize_text_field($input['privacy']);

        if (isset($input['privacy_link_text']))
            $new_input['privacy_link_text'] = sanitize_text_field($input['privacy_link_text']);

        if (isset($input['privacy_text']))
            $new_input['privacy_text'] = sanitize_text_field($input['privacy_text']);		
		
		// SMS Settings *********************************************************************************
		
		if (isset($input['send_sms_customer']))
            $new_input['send_sms_customer'] = $input['send_sms_customer'];
		
		if (isset($input['sms_account_id']))
            $new_input['sms_account_id'] = sanitize_text_field($input['sms_account_id']);
		
		if (isset($input['sms_user_email']))
            $new_input['sms_user_email'] = sanitize_text_field($input['sms_user_email']);
		
		if (isset($input['sms_user_password']))
            $new_input['sms_user_password'] = sanitize_text_field($input['sms_user_password']);
		
		if (isset($input['sms_message']))
            $new_input['sms_message'] = sanitize_textarea_field($input['sms_message']);
		
		// Work Hours Settings **************************************************************************
		
		if (isset($input['opening_hours']))
            $new_input['opening_hours'] = sanitize_text_field($input['opening_hours']);
		
		if (isset($input['lunch_starts']))
            $new_input['lunch_starts'] = sanitize_text_field($input['lunch_starts']);
		
		if (isset($input['lunch_ends']))
            $new_input['lunch_ends'] = sanitize_text_field($input['lunch_ends']);
		
		if (isset($input['closing_hours']))
            $new_input['closing_hours'] = sanitize_text_field($input['closing_hours']);
		
		if (isset($input['closing_hours_friday']))
            $new_input['closing_hours_friday'] = sanitize_text_field($input['closing_hours_friday']);		
		
		// Log Settings *********************************************************************************
		
		if (isset($input['record_leads']))
            $new_input['record_leads'] = $input['record_leads'];
		
		if (isset($input['send_email_lead_fail']))
            $new_input['send_email_lead_fail'] = $input['send_email_lead_fail'];	
		
		if (isset($input['email_lead_fail']))
            $new_input['email_lead_fail'] = sanitize_text_field($input['email_lead_fail']);
		
		if (isset($input['ignore_customers_lead_fail']))
            $new_input['ignore_customers_lead_fail'] = sanitize_text_field($input['ignore_customers_lead_fail']);		
		
        return $new_input;
    }

    /**
     * Global Default Settings
     */
    public function print_section_info() {
        print 'Enter your global default settings below, these can be overrode by the values in the shortcode:';
    }
	
	public function geturl_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="geturl" name="alcapiform_option_name[geturl]" value="%s" />', isset($this->options['geturl']) ? esc_attr($this->options['geturl']) : 'http://applicationform.hatpacks.com.au/api/enquiryform'
        );
    }

    public function posturl_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="posturl" name="alcapiform_option_name[posturl]" value="%s" />', isset($this->options['posturl']) ? esc_attr($this->options['posturl']) : 'http://applicationform.hatpacks.com.au/api/enquiryform/post'
        );
    }

    public function cid_callback() {
        printf(
                '<input style="width: 80px;" type="text" id="cid" name="alcapiform_option_name[cid]" value="%s" />', isset($this->options['cid']) ? esc_attr($this->options['cid']) : ''
        );
    }

    public function css_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="css" name="alcapiform_option_name[css]" value="%s" />', isset($this->options['css']) ? esc_attr($this->options['css']) : '/css/api-enquiry-form.css'
        );
    }

    public function jquery_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="css" name="alcapiform_option_name[jquery]" value="%s" />', isset($this->options['jquery']) ? esc_attr($this->options['jquery']) : ''
        );
    }    

    public function pssurl_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="css" name="alcapiform_option_name[pssurl]" value="%s" />', isset($this->options['pssurl']) ? esc_attr($this->options['pssurl']) : ''
        );
    }

    public function partner_email_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="css" name="alcapiform_option_name[partner_email]" value="%s" />', isset($this->options['partner_email']) ? esc_attr($this->options['partner_email']) : ''
        );
    }

    public function from_email_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="css" name="alcapiform_option_name[from_email]" value="%s" />', isset($this->options['from_email']) ? esc_attr($this->options['from_email']) : ''
        );
    }

    public function company_name_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="css" name="alcapiform_option_name[company_name]" value="%s" />', isset($this->options['company_name']) ? esc_attr($this->options['company_name']) : ''
        );
    }

    public function send_emails_callback() {
        printf(
                '<input type="checkbox" id="css" name="alcapiform_option_name[send_emails]" %s />', (isset($this->options["send_emails"]) && $this->options["send_emails"] == "on") ? "checked" : ""
        );
    }	
	
	/**
     * Form Settings
     */
    public function print_form_section() {
        print 'Enter the settings below to customize the fields shown in the enquiry form:';
    }
		
	public function ask_preferred_time_call_back_callback() {
        printf(
                '<input type="checkbox" id="css" name="alcapiform_option_name[ask_preferred_time_call_back]" %s />', (isset($this->options["ask_preferred_time_call_back"]) && $this->options["ask_preferred_time_call_back"] == "on") ? "checked" : ""
        );
    }
	
	public function privacy_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="css" name="alcapiform_option_name[privacy]" value="%s" />', isset($this->options['privacy']) ? esc_attr($this->options['privacy']) : ''
        );
    }

    public function privacy_link_text_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="css" name="alcapiform_option_name[privacy_link_text]" value="%s" />', isset($this->options['privacy_link_text']) ? esc_attr($this->options['privacy_link_text']) : ''
        );
    }

    public function privacy_text_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="css" name="alcapiform_option_name[privacy_text]" value="%s" />', isset($this->options['privacy_text']) ? esc_attr($this->options['privacy_text']) : ''
        );
    }
	
	/**
     * SMS Settings
     */
    public function print_sms_section() {
        print 'Enter the settings below to trigger SMS (via Esendex) on lead submissions, it works only for those cases where it redirects to the "Complete" page:';
    }
	
	public function send_sms_customer_callback() {
        printf(
                '<input type="checkbox" id="css" name="alcapiform_option_name[send_sms_customer]" %s />', (isset($this->options["send_sms_customer"]) && $this->options["send_sms_customer"] == "on") ? "checked" : ""
        );
    }
	
	public function sms_account_id_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="css" name="alcapiform_option_name[sms_account_id]" value="%s" />', isset($this->options['sms_account_id']) ? esc_attr($this->options['sms_account_id']) : ''
        );
    }
	
	public function sms_user_email_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="css" name="alcapiform_option_name[sms_user_email]" value="%s" />', isset($this->options['sms_user_email']) ? esc_attr($this->options['sms_user_email']) : ''
        );
    }
	
	public function sms_user_password_callback() {
        printf(
                '<input style="width: 700px;" type="password" id="css" name="alcapiform_option_name[sms_user_password]" value="%s" />', isset($this->options['sms_user_password']) ? esc_attr($this->options['sms_user_password']) : ''
        );
    }
	
	public function sms_message_callback() {
        printf(
                '<textarea style="width: 700px; height: 240px;" id="css" name="alcapiform_option_name[sms_message]">%s</textarea>', isset($this->options['sms_message']) ? esc_attr($this->options['sms_message']) : ''
        );
    }
	
	/**
     * Working Hours Settings
     */
    public function print_working_hours_section() {
        print 'Enter the settings below according to your business hours, be aware that changes here affect the schedule of SMS:';
    }
	
	public function opening_hours_callback() {
        printf(
                '<input style="width: 120px;" type="time" id="css" name="alcapiform_option_name[opening_hours]" value="%s" />', isset($this->options['opening_hours']) ? esc_attr($this->options['opening_hours']) : ''
        );
    }

	public function lunch_starts_callback() {
        printf(
                '<input style="width: 120px;" type="time" id="css" name="alcapiform_option_name[lunch_starts]" value="%s" />', isset($this->options['lunch_starts']) ? esc_attr($this->options['lunch_starts']) : ''
        );
    }
	
	public function lunch_ends_callback() {
        printf(
                '<input style="width: 120px;" type="time" id="css" name="alcapiform_option_name[lunch_ends]" value="%s" />', isset($this->options['lunch_ends']) ? esc_attr($this->options['lunch_ends']) : ''
        );
    }
	
	public function closing_hours_callback() {
        printf(
                '<input style="width: 120px;" type="time" id="css" name="alcapiform_option_name[closing_hours]" value="%s" />', isset($this->options['closing_hours']) ? esc_attr($this->options['closing_hours']) : ''
        );
    }
	
	public function closing_hours_friday_callback() {
        printf(
                '<input style="width: 120px;" type="time" id="css" name="alcapiform_option_name[closing_hours_friday]" value="%s" />', isset($this->options['closing_hours_friday']) ? esc_attr($this->options['closing_hours_friday']) : ''
        );

		// Additional info
		print('</br></br>');
		print('<p><strong>SMS Schedule Rules:</strong></p>');
		print('</br>');
		print('</ul>');
		
		printf(               
				'<li>When Mondays to Fridays before <strong>%s</strong> -> Schedule SMS at <strong>%s</strong> on the Same Day</li>',
				isset($this->options['opening_hours']) ? esc_attr(date('h:i A', strtotime($this->options['opening_hours']))) : '',
				isset($this->options['opening_hours']) ? esc_attr(date('h:i A', strtotime($this->options['opening_hours']))) : ''
        );
		printf(               
				'<li>When Lunch time -> Schedule SMS at <strong>%s</strong> on the Same Day</li>',
				isset($this->options['lunch_ends']) ? esc_attr(date('h:i A', strtotime($this->options['lunch_ends']))) : ''
        );
		printf(               
				'<li>When Mondays to Thursdays after <strong>%s</strong> -> Schedule SMS at <strong>%s</strong> on the Following Day</li>',
				isset($this->options['closing_hours']) ? esc_attr(date('h:i A', strtotime($this->options['closing_hours']))) : '',
				isset($this->options['opening_hours']) ? esc_attr(date('h:i A', strtotime($this->options['opening_hours']))) : ''
        );		
		printf(               
				'<li>When Saturdays or Sundays at anytime or Fridays after <strong>%s</strong> -> Schedule SMS at <strong>%s</strong> on Monday</li>',
				isset($this->options['closing_hours_friday']) ? esc_attr(date('h:i A', strtotime($this->options['closing_hours_friday']))) : '',
				isset($this->options['opening_hours']) ? esc_attr(date('h:i A', strtotime($this->options['opening_hours']))) : ''
        );
		print('<li>Otherwise, it sends it right away</li>');
		print('</ul>');
    }
	
	/**
     * Log Settings
     */
    public function print_log_section() {
        print 'Enter the settings below to record every lead entry (log) and report failures (email):';
    }
	
	public function record_leads_callback() {
        printf(
                '<input type="checkbox" id="css" name="alcapiform_option_name[record_leads]" %s />', (isset($this->options["record_leads"]) && $this->options["record_leads"] == "on") ? "checked" : ""
        );
    }
	
	public function send_email_lead_fail_callback() {
        printf(
                '<input type="checkbox" id="css" name="alcapiform_option_name[send_email_lead_fail]" %s />', (isset($this->options["send_email_lead_fail"]) && $this->options["send_email_lead_fail"] == "on") ? "checked" : ""
        );
    }

	public function email_lead_fail_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="css" name="alcapiform_option_name[email_lead_fail]" value="%s" />', isset($this->options['email_lead_fail']) ? esc_attr($this->options['email_lead_fail']) : ''
        );
    }
	
	public function ignore_customers_lead_fail_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="css" name="alcapiform_option_name[ignore_customers_lead_fail]" value="%s" />', isset($this->options['ignore_customers_lead_fail']) ? esc_attr($this->options['ignore_customers_lead_fail']) : ''
        );
    }

    /**
     * Usage Instruction
     */
    public function print_section_info_usage() {
        print 'Use shortcode to display ALC API Form, as below: <br/>
    	<pre>
    		[alcapi_contact_form]
    	</pre>
    	';
    }

    /**
     * Override Instruction
     */
    public function print_section_info_shortcode() {
        print 'The global default values above can be overrode by the shortcode usage, as below: <br/>
    	<pre>
    		[alcapi_contact_form geturl="http://applicationform.hatpacks.com.au/api/enquiryform"
    		posturl="http://applicationform.hatpacks.com.au/api/enquiryform/post"
    		cid="ALC"
    		css="https://www.australianlendingcentre.com.au/css/api-enquiry-form.css"]
    	</pre>
    	';
    } 	
}
?>